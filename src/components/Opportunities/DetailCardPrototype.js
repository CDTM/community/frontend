import React from "react";
import { Card } from "antd";

const DetailCardPrototype = ({ title }) => {
  return (
    <Card title={title}>
      <p>Something within the card</p>
    </Card>
  );
};

export default DetailCardPrototype;
