export default class Talent {
  constructor({
    cdtmAffiliation = "",
    createdAt = "",
    createdBy = "",
    description = "",
    level = "",
    location = "",
    role = [],
    name = "",
  }) {
    this.cdtmAffiliation = cdtmAffiliation;
    this.createdAt = createdAt;
    this.createdBy = createdBy;
    this.description = description;
    this.level = level;
    this.location = location;
    this.role = role;
    this.name = name;
  }
}
