import React from "react";
import DetailCardPrototype from "../../DetailCardPrototype";

const DetailCard = () => {
  const title = "Biggest talent ever";

  return <DetailCardPrototype title={title} />;
};

export default DetailCard;
