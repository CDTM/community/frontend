import React, { useState } from "react";
import {
  Form,
  Input,
  Select,
  Button,
  Upload,
  AutoComplete,
  message,
} from "antd";
import { UploadOutlined } from "@ant-design/icons";
import { compose } from "recompose";
import { withFirebase } from "../../../Firebase";
import cities from "../../../../constants/worldcities";
import {
  opportunityRoleOptions,
  opportunityLevelOptions,
} from "../../../../constants/opportunities";

const { Option } = Select;
const { TextArea } = Input;

const layout = {
  labelCol: {
    span: 32,
  },
  wrapperCol: {
    span: 32,
  },
};
const tailLayout = {
  wrapperCol: { offset: 12, span: 32 },
};

function JobForm(props) {
  const [location, setLocation] = useState("");
  const [options, setOptions] = useState([]);
  const [logoURL, setlogoURL] = useState("");
  const [fileUploaded, setFileUploaded] = useState("");
  const [numberUploads, setNumberUploads] = useState(0);
  const [form] = Form.useForm();

  const { firebase, authUser, ReloadPosts, closeDrawer } = props;

  const onFinish = values => {
    firebase
      .doCreateJob(values, authUser)
      .then(() => {
        ReloadPosts();
        closeDrawer();
        message.success("Job was added successfully!");
      })
      .catch(() => {
        message.error("Error in firebase. Please contact admins.");
      });
  };

  const onFinishFailed = ({ errorFields }) => {
    form.scrollToField(errorFields[0].name);
  };

  const onSearchLocation = searchText => {
    const potentialOptions = cities.filter(word => word.includes(searchText));
    const newOptions = potentialOptions.map(word => ({
      value: word,
    }));

    setOptions(!searchText ? [] : newOptions);
  };

  const onSelectLocation = data => {
    setLocation(data);
  };

  const onChangeLocation = data => {
    setLocation(data);
  };

  const onChangelogoURL = event => {
    setlogoURL(event.target.value);
  };

  const propsUpload = {
    name: "job_description",
    headers: {
      authorization: "authorization-text",
    },
    accept: ".pdf",
    multiple: false,
    beforeUpload(file, fileList) {
      if (numberUploads === 1) {
        message.error("Only one file upload is allowed.");
        fileList.pop();
        return false;
      }
      return true;
    },
    onChange(info) {
      if (info.file.status === "done") {
        setFileUploaded(info.file.uid);
        props.setFileUploaded(info.file.uid);
        setNumberUploads(1);
        message.success("Document was upload successfully.");
      } else if (info.file.status === "error") {
        setNumberUploads(0);
        message.error("Document was not uploaded.");
      } else if (info.file.status === "removed") {
        setFileUploaded("");
        props.setFileUploaded("");
        setNumberUploads(0);
        firebase
          .removeOpportunityPDF("job", authUser, fileUploaded)
          .then(() => {
            message.success("Document was removed successfully.");
          })
          .catch(() => {
            message.error("Document was not removed successfully.");
          });
      }
    },
    customRequest({ onError, onSuccess, file, onProgress }) {
      firebase.uploadOpportunityPDF(
        "job",
        onError,
        onSuccess,
        onProgress,
        file,
        authUser,
      );
    },
  };

  return (
    <React.Fragment>
      <h1>Submit a new job</h1>

      <Form
        {...layout}
        layout="vertical"
        name="basic"
        initialValues={{
          role: "Software Engineering",
          level: "Working student",
          linkToJob: "",
          websiteURL: "",
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <Form.Item
          label="Company name:"
          name="company"
          rules={[
            { required: true, message: "Name of your company is missing" },
          ]}
          placeholder="Mostly Awesome Company"
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Company website URL"
          name="websiteURL"
          rules={[{ required: false }]}
        >
          <Input placeholder="https://yourcompany.com" />
        </Form.Item>

        <Form.Item
          label="Link to company logo (optional)"
          name="logoURL"
          rules={[{ required: false }]}
        >
          <Input
            placeholder="https://google.com/logo.png"
            onChange={onChangelogoURL}
          />
        </Form.Item>

        <span
          style={{
            maxWidth: "250px",
          }}
        >
          {logoURL && (
            <img
              src={logoURL}
              alt="Company Logo could not be fetched. Please use a different URL."
              style={{
                maxWidth: "100%",
                maxHeight: "144px",
                paddingTop: "22px ",
                paddingLeft: "20px",
              }}
            />
          )}
        </span>

        <Form.Item
          label="Role"
          name="role"
          rules={[{ required: true, message: "" }]}
        >
          <Select style={{ width: 200 }}>
            {opportunityRoleOptions.map(element => {
              return <Option value={element}>{element}</Option>;
            })}
          </Select>
        </Form.Item>

        <Form.Item
          label="Level"
          name="level"
          rules={[{ required: true, message: "" }]}
        >
          <Select style={{ width: 200 }}>
            {opportunityLevelOptions.map(element => {
              return <Option value={element}>{element}</Option>;
            })}
          </Select>
        </Form.Item>

        <Form.Item
          label="Location (Name of city)"
          name="location"
          rules={[{ required: true, message: "Location is missing" }]}
        >
          <AutoComplete
            value={location}
            options={options}
            style={{
              width: 200,
            }}
            onSelect={onSelectLocation}
            onSearch={onSearchLocation}
            onChange={onChangeLocation}
            placeholder="Munich"
          />
        </Form.Item>

        <Form.Item
          label="Link to the job description (optional)"
          name="linkToJob"
          rules={[{ required: false, type: "url" }]}
        >
          <Input placeholder="https://www.company.de/yourjob" />
        </Form.Item>

        <Form.Item
          label="Description"
          name="description"
          rules={[{ required: true, message: "Description is missing" }]}
        >
          <TextArea
            rows={4}
            placeholder="We are looking for a junior front-end developer for our React-Native mobile application."
          />
        </Form.Item>

        <Form.Item label="Job description as PDF (optional)" name="pdfFile">
          <Upload {...propsUpload}>
            <Button>
              <UploadOutlined /> Click to Upload
            </Button>
          </Upload>
        </Form.Item>

        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
    </React.Fragment>
  );
}

const WrappedDrawer = compose(withFirebase)(JobForm);

export default WrappedDrawer;
