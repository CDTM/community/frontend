import React, { Component } from "react";
import { isMobileOnly } from "react-device-detect";

import {
  Layout,
  PageHeader,
  Tabs,
  Button,
  Row,
  Col,
  Empty,
  Drawer,
  Dropdown,
  Menu,
  Spin,
  Tooltip,
  Modal,
  List,
  Checkbox,
  Space,
} from "antd";

import {
  InfoCircleOutlined,
  PlusOutlined,
  SortAscendingOutlined,
  MenuOutlined,
  Loading3QuartersOutlined,
  FilterOutlined,
} from "@ant-design/icons";
import { Swipeable } from "react-swipeable";

import InfiniteScroll from "react-infinite-scroller";

import { withAuthorization } from "../Session";
import { withFirebase } from "../Firebase";
import ResourcesItem from "./Item";
import ResourcesSubmit from "./Submit";

import "./index.css";

import { resourceTypes, sortOptions } from "../../constants/resources";
import { weekNumber, weekYear } from "./dateFunctions";

class Resources extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: null,
      sortBy: "score",
      queryLimit: 18,
      loading: false,
      hasMore: true,
      queryCurrentLastResource: null,
      data: [],
      resourcesSubmitDrawerOpen: false,
      filter: [],
      indeterminate: false,
      checkAll: false,
      visibleFilter: false,
    };
  }

  componentDidMount = () => {
    this.onTabChange("Feed");
  };

  onTabChange = newActiveTab => {
    const { activeTab } = this.state;
    const { firebase } = this.props;
    if (newActiveTab === activeTab) return;

    this.setState(
      {
        activeTab: newActiveTab,
        filter: [],
        indeterminate: false,
        checkAll: false,
        data: [],
        hasMore: true,
        queryCurrentLastResource: null,
      },
      () => {
        firebase.logEvent("resource_change_tab", { tab: newActiveTab });
      },
    );
  };

  sortBy = by => {
    this.setState({
      sortBy: by.key,
      data: [],
      hasMore: true,
      queryCurrentLastResource: null,
    });
  };

  onChangeFilter = filter => {
    const { activeTab } = this.state;
    this.setState({
      filter,
      indeterminate:
        !!filter.length && filter.length < resourceTypes[activeTab].tags.length,
      checkAll: filter.length === resourceTypes[activeTab].tags.length,
      data: [],
      hasMore: true,
      queryCurrentLastResource: null,
    });
  };

  onCheckAllChange = e => {
    const { activeTab } = this.state;
    this.setState({
      filter: e.target.checked ? resourceTypes[activeTab].tags : [],
      indeterminate: false,
      checkAll: e.target.checked,
      data: [],
      hasMore: true,
      queryCurrentLastResource: null,
    });
  };

  reloadResources = () => {
    this.setState({
      hasMore: true,
      queryCurrentLastResource: null,
      data: [],
    });
  };

  // This function is called on initial load and whenever a tab, filter or sort changes
  loadNewResources = async () => {
    const {
      activeTab,
      sortBy,
      data,
      queryCurrentLastResource,
      queryLimit,
      filter,
    } = this.state;
    // Use to check whether tab has changed when changing state at the end of async execution
    const targetActiveTab = activeTab;

    const { firebase } = this.props;
    this.setState({ loading: true });

    let query = firebase.resources();

    // Distinction between Feed, Score Sorting, All other with filter, All other without Filter
    if (activeTab === "Feed") {
      // Feed is simply ordered after time desc
      query = query.orderBy("createdAt", "desc");
    } else if (sortBy === "score") {
      /* For score sorting I couldnt directly do a firebase query
      However computing the score in frontend would require loading all resources
      Therfore I implemented a firebase function that loads all respective resources, computes the score and only returns the query size batch 
      This has then nothing to do with the query that is called for all other cases */
      const newResources = await firebase.relevanceRankedResources(
        activeTab,
        filter,
        data.length,
        queryLimit,
      );
      // If no new docs left, return
      if (newResources.length === 0) {
        this.setState(
          prevState =>
            targetActiveTab === prevState.activeTab && {
              hasMore: false,
              loading: false,
            },
        );
      } else {
        // Add the new resources to data and update the queryCurrentLastResource property
        this.setState(
          prevState =>
            targetActiveTab === prevState.activeTab && {
              data: [...data, ...newResources],
              queryCurrentLastResource: newResources[newResources.length - 1],
              loading: false,
            },
        );
      }
      return;
    } else if (filter.length === 0 || filter.length > 10) {
      // default if no filter is activated
      query = query
        .where("type", "==", activeTab)
        .orderBy(sortBy, sortOptions[sortBy].order);
    } else {
      // default when filter is activated
      query = query
        .where("type", "==", activeTab)
        .where("tags", "array-contains-any", filter)
        .orderBy(sortBy, sortOptions[sortBy].order);
    }

    // Check if an initial load has already happend, then call the query
    if (data.length > 0) query = query.startAfter(queryCurrentLastResource);
    const querySnapshot = await query.limit(queryLimit).get();

    // If no new docs left, return
    if (querySnapshot.size === 0) {
      this.setState(
        prevState =>
          targetActiveTab === prevState.activeTab && {
            hasMore: false,
            loading: false,
          },
      );
    } else {
      // Bring resources in the respective format, not sure why it has to look exactly like this
      const newResources = [];
      querySnapshot.forEach(doc => {
        const resource = {
          id: doc.id,
          ref: doc.ref,
          ...doc.data(),
        };
        newResources.push(resource);
      });

      // Add the new resources to data and update the queryCurrentLastResource property
      this.setState(
        prevState =>
          targetActiveTab === prevState.activeTab && {
            data: [...data, ...newResources],
            queryCurrentLastResource:
              querySnapshot.docs[querySnapshot.docs.length - 1],
            loading: false,
          },
      );
    }
  };

  handleVisibleChangeFilter = flag => {
    this.setState({ visibleFilter: flag });
  };

  openResourcesSubmitDrawer = () => {
    this.setState({
      resourcesSubmitDrawerOpen: true,
    });
  };

  closeResourcesSubmitDrawer = () => {
    this.setState({
      resourcesSubmitDrawerOpen: false,
    });
  };

  showInfoModal = () => {
    this.setState({
      infoModalVisible: true,
    });
  };

  closeInfoModal = () => {
    this.setState({
      infoModalVisible: false,
    });
  };

  swipe = (event, isMobile) => {
    if (
      (isMobile &&
        event.dir === "Down" &&
        event.absY >= 140 &&
        event.velocity >= 1.5) ||
      (!isMobile &&
        event.dir === "Right" &&
        event.absX >= 160 &&
        event.velocity >= 1.5)
    ) {
      this.setState({
        resourcesSubmitDrawerOpen: false,
      });
    }
  };

  render() {
    const {
      activeTab,
      data,
      hasMore,
      sortBy,
      resourcesSubmitDrawerOpen,
      loading,
      infoModalVisible,
      indeterminate,
      checkAll,
      filter,
      visibleFilter,
    } = this.state;
    const { windowWidth, authUser } = this.props;
    return (
      <Layout className="module">
        <PageHeader
          title="Resources"
          id="resources-header"
          subTitle={[
            isMobileOnly ? "Discover" : "Discover and share",
            <InfoCircleOutlined
              onClick={this.showInfoModal}
              style={{ paddingLeft: "8px" }}
              key="resource-info-icon"
            />,
            <Modal
              title="Resources"
              visible={infoModalVisible}
              onCancel={() => this.closeInfoModal()}
              key="resource-info-modal"
              footer={[
                <Button
                  key="close"
                  type="primary"
                  onClick={() => this.closeInfoModal()}
                >
                  Close
                </Button>,
              ]}
            >
              <p>
                A collection of our favorite resources: Discover content
                submitted by others, explore new tools to increase your
                productivity, find a new book to read or podcast to listen to,
                learn something new with recommended online courses, or find a
                conference to attend.
              </p>
              <p>
                Submit your own resources und upvote your favorite resources
                shared by others. Founders can promote their own product and
                offer student discounts.
              </p>
            </Modal>,
          ]}
          className={`page-header ${isMobileOnly ? "mobile-page-header" : ""}`}
          extra={[
            <Tooltip
              key="submitTooltip"
              placement="left"
              title="Submit new resource"
            >
              <Button
                key="Submit"
                icon={<PlusOutlined />}
                onClick={this.openResourcesSubmitDrawer}
                type="primary"
                {...(isMobileOnly ? { shape: "circle" } : {})}
              >
                {isMobileOnly ? "" : "Submit new Resource"}
              </Button>
            </Tooltip>,
            activeTab !== "Feed" && activeTab !== null && (
              <Dropdown
                key="filter"
                onVisibleChange={this.handleVisibleChangeFilter}
                visible={visibleFilter}
                overlay={
                  <div
                    style={{
                      backgroundColor: "white",
                      padding: 10,
                      borderRadius: 3,
                    }}
                  >
                    <Space direction="vertical">
                      <Checkbox
                        indeterminate={indeterminate}
                        onChange={this.onCheckAllChange}
                        checked={checkAll}
                        style={{ fontWeight: "bold" }}
                        defaultChecked={false}
                      >
                        Check all
                      </Checkbox>
                      <Checkbox.Group
                        onChange={this.onChangeFilter}
                        value={filter}
                      >
                        <Space direction="vertical">
                          {resourceTypes[activeTab].tags.map(option => {
                            return (
                              <Checkbox key={option} value={option}>
                                {option}
                              </Checkbox>
                            );
                          })}
                        </Space>
                      </Checkbox.Group>
                    </Space>
                  </div>
                }
              >
                {isMobileOnly ? (
                  <Button icon={<FilterOutlined />} shape="circle" />
                ) : (
                  <Button icon={<FilterOutlined />}>Filter By</Button>
                )}
              </Dropdown>
            ),
            activeTab && activeTab !== "Feed" && (
              <Dropdown
                key="sort"
                overlay={
                  <Menu onClick={this.sortBy}>
                    {Object.keys(sortOptions).map(key => (
                      <Menu.Item key={key}>
                        {sortOptions[key].icon}
                        {sortOptions[key].label}
                      </Menu.Item>
                    ))}
                  </Menu>
                }
              >
                {isMobileOnly ? (
                  <Button
                    key="Sort"
                    icon={<SortAscendingOutlined />}
                    shape="circle"
                  />
                ) : (
                  <Button.Group key="SortGroup" style={{ marginLeft: "8px" }}>
                    <Button>Sort By</Button>
                    <Button>{sortOptions[sortBy].icon}</Button>
                  </Button.Group>
                )}
              </Dropdown>
            ),
          ]}
        />
        <Tabs
          onChange={this.onTabChange}
          activeKey={activeTab}
          size="default"
          style={{
            marginTop: isMobileOnly ? "70px" : "0px",
          }}
          className={isMobileOnly ? "resources-tabs" : null}
        >
          <Tabs.TabPane
            key="Feed"
            tab={
              <span>
                <MenuOutlined style={{ paddingLeft: "24px" }} />
                Feed
              </span>
            }
          />
          {Object.keys(resourceTypes).map(type => (
            <Tabs.TabPane
              tab={
                resourceTypes[type].tooltip ? (
                  <Tooltip
                    placement="right"
                    title={resourceTypes[type].tooltip}
                  >
                    {type}
                    <InfoCircleOutlined style={{ paddingLeft: "8px" }} />
                  </Tooltip>
                ) : (
                  type
                )
              }
              key={type}
            />
          ))}
        </Tabs>
        <Layout
          className="module-thin"
          style={{ marginTop: isMobileOnly ? "115px" : "0px" }}
        >
          {activeTab && (
            <ResourceItemsWrapper
              data={data}
              authUser={authUser}
              activeTab={activeTab}
              loadNewResources={() => this.loadNewResources()}
              reloadResources={() => this.reloadResources()}
              hasMore={hasMore}
              loading={loading}
            />
          )}
        </Layout>
        <Drawer
          placement={isMobileOnly ? "bottom" : "right"}
          visible={resourcesSubmitDrawerOpen}
          onClose={this.closeResourcesSubmitDrawer}
          closable
          destroyOnClose
          width={Math.min(windowWidth * 0.85, 750)}
          height="85%"
        >
          <Swipeable onSwiped={event => this.swipe(event, isMobileOnly)}>
            <ResourcesSubmit
              authUser={authUser}
              closeResourcesSubmitDrawer={() =>
                this.closeResourcesSubmitDrawer()
              }
              refreshResources={() => this.loadNewResources()}
            />
          </Swipeable>
        </Drawer>
      </Layout>
    );
  }
}

const ResourceItemsWrapper = ({
  data,
  authUser,
  activeTab,
  loadNewResources,
  reloadResources,
  hasMore,
  loading,
}) => {
  if (!hasMore && data.length === 0)
    return (
      <Empty
        image={Empty.PRESENTED_IMAGE_SIMPLE}
        description="Be the first one to submit a resource in this category!"
      />
    );

  const renderItem = resourceData => (
    <ResourcesItem
      resource={resourceData}
      authUser={authUser}
      key={resourceData.id}
      refreshResources={reloadResources}
      isFeed={activeTab === "Feed"}
      isMobileOnly={isMobileOnly}
    />
  );

  // Group resources by date for Feed tab
  if (activeTab === "Feed") {
    const groupedResources = {};
    data.forEach(resource => {
      const date = resource.createdAt.toDate();
      const dateStr = `${weekYear(date)}-${weekNumber(date)}`;

      if (!Object.keys(groupedResources).includes(dateStr))
        groupedResources[dateStr] = [];

      groupedResources[dateStr].push(resource);
    });

    return (
      <InfiniteScroll
        pageStart={0} // uses pages, not compatible with firebase
        loadMore={() => loadNewResources()}
        hasMore={!loading && hasMore}
      >
        <Row
          gutter={{ xs: 4, sm: 14, md: 14, lg: 14 }}
          style={{
            display: "flex",
            justifyContent: "center",
          }}
          type="flex"
        >
          <Col xs={24} sm={22} md={20} lg={18} xl={14} xxl={12}>
            {Object.keys(groupedResources).map(dateStr => {
              const splitDateStr = dateStr.split("-");
              return (
                <React.Fragment key={dateStr}>
                  <h1 className="resources-feed-heading">
                    {`Week ${splitDateStr[1]} of ${splitDateStr[0]}`}
                  </h1>
                  {groupedResources[dateStr].map(renderItem)}
                </React.Fragment>
              );
            })}
          </Col>
        </Row>
        {loading && hasMore && (
          <Spin
            indicator={<Loading3QuartersOutlined spin />}
            style={{
              top: "30px",
              width: "100%",
              textAlign: "center",
            }}
          />
        )}
      </InfiniteScroll>
    );
  }

  // For all other tabs
  return (
    <InfiniteScroll
      pageStart={0} // uses pages, not compatible with firebase
      loadMore={() => loadNewResources()}
      hasMore={!loading && hasMore}
    >
      {data.length > 0 && (
        <List
          dataSource={data}
          grid={{
            xs: 1,
            sm: 2,
            md: 3,
            lg: 4,
            xl: 4,
            xxl: 5,
          }}
          renderItem={renderItem}
        />
      )}
      {loading && hasMore && (
        <Spin
          indicator={<Loading3QuartersOutlined spin />}
          style={{
            top: "30px",
            width: "100%",
            textAlign: "center",
          }}
        />
      )}
    </InfiniteScroll>
  );
};

const condition = authUser => authUser != null;

export default withFirebase(withAuthorization(condition)(Resources));
