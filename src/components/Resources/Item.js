import React, { Component } from "react";
import { withFirebase } from "../Firebase";
import "./Item.css";
import ResourcePopup from "./ResourcePopup";
import ResourceCard from "./Card/ResourceCard"

class ResourcesCard extends Component {
  constructor(props) {
    super(props);
    this.openDescriptionModal = this.openDescriptionModal.bind(this)
    this.closeDescriptionModal = this.closeDescriptionModal.bind(this)
  
    this.state = {
      resourcePopupOpen: false,
      upvoted: false,
      processUpvote: false,
      upvotes: 0,
    }

  }

  componentDidMount(){
    const { resource, authUser } = this.props;
     
    this.setState({
      upvotes: resource.upvotes,
      upvoted: resource.upvoters && resource.upvoters.includes(authUser.email),
    })
  }

  upvote = async (event) => {
    event.stopPropagation();
    const { firebase, resource, authUser } = this.props;
    const { upvoted, upvotes } = this.state
    this.setState({processUpvote: true})
    const upvoteRef = resource.ref.collection("upvotes").doc(authUser.email);

    if (upvoted) {
      await upvoteRef.delete();
      this.setState({
        processUpvote: false,
        upvotes: upvotes - 1,
        upvoted: false,
      })
      firebase.logEvent("resources_downvoted", {
        resourceType: resource.type,
        resourceId: resource.id,
      });
    } else {
      await upvoteRef.set({});
      this.setState({
        processUpvote: false,
        upvotes: upvotes + 1,
        upvoted: true,
      })
      firebase.logEvent("resources_upvoted", {
        resourceType: resource.type,
        resourceId: resource.id,
      });
    }
  };

  openDescriptionModal = () => {
    const { firebase, resource } = this.props;
    this.setState({
      resourcePopupOpen: true,
    });
    firebase.logEvent("resources_open_description_modal", {
      resourceType: resource.type,
      resourceId: resource.id,
    });
  };

  closeDescriptionModal = () => {
    this.setState({
      resourcePopupOpen: false,
    });
  };

  render() {
    const { 
      authUser, 
      isFeed, 
      resource, 
      isMobileOnly, 
      refreshResources,
      firebase,
    } = this.props;
    const { resourcePopupOpen, processUpvote, upvoted, upvotes } = this.state;

    return (
      <React.Fragment>
        {isFeed ? (
          <div style={{ marginBottom: "14px" }}>
            <ResourceCard
              isFeed={isFeed} 
              isMobileOnly={isMobileOnly}
              resource={resource}
              authUser={authUser}
              firebase={firebase}
              upvoted={upvoted}
              upvote={this.upvote}
              upvotes={upvotes}
              processUpvote={processUpvote}
              refreshResources={refreshResources}
              openDescriptionModal={this.openDescriptionModal}
            />
          </div>
        ) : (
            <ResourceCard
              isFeed={isFeed} 
              isMobileOnly={isMobileOnly}
              resource={resource}
              authUser={authUser}
              firebase={firebase}
              upvoted={upvoted}
              upvote={this.upvote}
              upvotes={upvotes}
              processUpvote={processUpvote}
              refreshResources={refreshResources}
              openDescriptionModal={this.openDescriptionModal}
            />
        )}
        {resourcePopupOpen && (
          <ResourcePopup
            resource={resource}
            authUser={authUser}
            upvoted={upvoted}
            upvote={this.upvote}
            upvotes={upvotes}
            processUpvote={processUpvote}
            close={this.closeDescriptionModal}
          />
        )}
      </React.Fragment>
    );
  }
}

export default withFirebase(ResourcesCard);
