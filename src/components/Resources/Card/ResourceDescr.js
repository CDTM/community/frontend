import React from "react";
import { Typography } from "antd";

const ResourceDescr = ({ resource }) => {
  return (
    <div
      style={{
        marginTop: "1.3rem",
        height: "5rem",
      }}
    >
      <Typography.Paragraph
        ellipsis={{ rows: 3 }}
      >
        {resource.description && resource.description}
      </Typography.Paragraph>
    </div>
  );
};

export default ResourceDescr;
