import React from "react";
import { Tooltip } from "antd";
import PercentageOutlined from "@ant-design/icons/PercentageOutlined";

const StudentDiscountIcon = () => (
  <Tooltip title="Student discount available">
    <PercentageOutlined
      className="action-icon"
      style={{ marginRight: "8px" }}
    />
  </Tooltip>
);

export default StudentDiscountIcon;