import React from "react";
// css imports
import "../Item.css";
// Self build components
import FeedCard from "./FeedCard";
import RegularCard from "./RegularCard";

const ResourceCard = ({
  isFeed,
  isMobileOnly,
  resource,
  authUser,
  firebase,
  refreshResources,
  openDescriptionModal,
  upvoted,
  upvote,
  processUpvote,
  upvotes,
}) => {

  return isFeed ? (
    <FeedCard
      {...{
        isMobileOnly,
        resource,
        upvoted,
        processUpvote,
        upvotes,
        openDescriptionModal,
        upvote,
        authUser,
        firebase,
        refreshResources,
      }}
    />
  ) : (
    <RegularCard
      {...{
        resource,
        isMobileOnly,
        openDescriptionModal,
        upvoted,
        upvotes,
        processUpvote,
        firebase,
        authUser,
        upvote,
        refreshResources,
      }}
    />
  );
};

export default ResourceCard;
