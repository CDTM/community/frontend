import React from "react";
// Ant Design
import { Card, Row, Col, Divider } from "antd";
// Own components
import ResourceImage from "./ResourceImage";
import ResourceTitle from "./ResourceTitle";
import ResourceDescr from "./ResourceDescr";
import ResourceTags from "./ResourceTags";
import ResourceDetails from "./ResourceDetails";
import UpvoteButtonGroup from "./UpvoteButtonGroup";
import DeleteIcon from "./DeleteIcon";
import GetButton from "./GetButton";
import StudentDiscountIcon from "./StudentDiscountIcon";

const RegularCard = ({
  resource,
  isMobileOnly,
  openDescriptionModal,
  upvoted,
  upvotes,
  processUpvote,
  firebase,
  authUser,
  upvote,
  refreshResources,
}) => {
  return (
    <Card hoverable onClick={openDescriptionModal} className="resources-card" key={resource.title}>
      <Row className="resources-card-meta">
        {resource.image && (
          <Col span={5}>
            <ResourceImage
              isFeed={false}
              isMobileOnly={isMobileOnly}
              image={resource.image}
            />
          </Col>
        )}
        <Col span={resource.image ? 19 : 24}>
          <ResourceTitle
            isFeed={false}
            isMobileOnly={isMobileOnly}
            resource={resource}
          />
        </Col>
      </Row>
      <ResourceDescr className="resources-card-descr"
        resource={resource}
      />
      {resource.tags && (
        <Row>
          <Col>
            <ResourceTags tags={resource.tags} />
          </Col>
        </Row>
      )}
      <Row>
        <Col span={24}>
          <ResourceDetails resource={resource} />
        </Col>
      </Row>
      <Divider style={{ margin: "10px 0 14px 0" }} />

      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <UpvoteButtonGroup
          upvoted={upvoted}
          title={resource.title}
          processUpvote={processUpvote}
          isFeed={false}
          upvotes={upvotes}
          commentsCount={resource.commentsCount}
          upvote={upvote}
        />
        <span>
          {resource.studentDiscount && <StudentDiscountIcon />}
          {resource.author &&
            authUser.email &&
            resource.author === authUser.email && (
              <DeleteIcon
                resource={resource}
                firebase={firebase}
                refreshResources={refreshResources}
              />
            )}
          <GetButton url={resource.url} />
        </span>
      </div>
    </Card>
  );
};
export default RegularCard;
