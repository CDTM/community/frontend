import React from "react";
// Ant Design
import { Row, Col, Card, Divider } from "antd";
// Own Components
import UpvoteButtonGroup from "./UpvoteButtonGroup";
import ResourceImage from "./ResourceImage";
import ResourceTitle from "./ResourceTitle";
import DeleteIcon from "./DeleteIcon";
import GetButton from "./GetButton";
import ResourceDescr from "./ResourceDescr";
import ResourceTags from "./ResourceTags";
import ResourceDetails from "./ResourceDetails";
import StudentDiscountIcon from "./StudentDiscountIcon";
// CSS
import "../Item.css";

const FeedCard = ({
  isMobileOnly,
  resource,
  upvoted,
  processUpvote,
  upvotes,
  openDescriptionModal,
  upvote,
  authUser,
  firebase,
  refreshResources,
}) => {
  return (
    <Card hoverable onClick={openDescriptionModal} className="resources-card" key={resource.title}>
      <Row className="resources-card-meta-feed">
        {!isMobileOnly && (
          <Col span={4}>
            <UpvoteButtonGroup
              upvoted={upvoted}
              title={resource.title}
              processUpvote={processUpvote}
              isFeed
              upvotes={upvotes}
              commentsCount={resource.commentsCount}
              openDescriptionModal={openDescriptionModal}
              upvote={upvote}
            />
          </Col>
        )}
        {resource.image && (
          <Col span={4}>
            <ResourceImage
              isFeed
              isMobileOnly={isMobileOnly}
              image={resource.image}
              onClick={openDescriptionModal}
            />
          </Col>
        )}
        <Col span={16}>
          <ResourceTitle
            isFeed
            isMobileOnly={isMobileOnly}
            openDescriptionModal={openDescriptionModal}
            resource={resource}
          />
          {!isMobileOnly && (
            <span style={{paddingLeft: 16}}>
              {resource.studentDiscount && <StudentDiscountIcon />}
              {resource.author &&
                authUser.email &&
                resource.author === authUser.email && (
                  <DeleteIcon {...{ resource, firebase, refreshResources }} />
                )}
            </span>
          )}
        </Col>
      </Row>

      {isMobileOnly && (
        <React.Fragment>
          <ResourceDescr
            openDescriptionModal={openDescriptionModal}
            resource={resource}
          />
          {resource.tags && <ResourceTags tags={resource.tags} />}
          <ResourceDetails resource={resource} />
          <Divider />

          <div>
            <UpvoteButtonGroup
              upvoted={upvoted}
              title={resource.title}
              processUpvote={processUpvote}
              isFeed
              upvotes={upvotes}
              commentsCount={resource.commentsCount}
              openDescriptionModal={openDescriptionModal}
              upvote={upvote}
            />
            <span>
              {resource.studentDiscount && <StudentDiscountIcon />}
              {resource.author &&
                authUser.email &&
                resource.author === authUser.email && (
                  <DeleteIcon
                    resource={resource}
                    firebase={firebase}
                    refreshResources={refreshResources}
                  />
                )}
              <GetButton url={resource.url} />
            </span>
          </div>
        </React.Fragment>
      )}
    </Card>
  );
};

export default FeedCard;
