import React from "react";
import { Button, Tooltip, Badge } from "antd";
import {
  MessageOutlined,
  CheckOutlined,
  LikeOutlined,
} from "@ant-design/icons";

const UpvoteButtonGroup = ({
  upvoted,
  title,
  processUpvote,
  isFeed = true,
  upvotes,
  commentsCount,
  openDescriptionModal,
  upvote,
}) => (
  <Button.Group>
    <Tooltip
      title={upvoted ? "Withdraw vote" : `Vote for ${title && title}`}
      placement="topLeft"
    >
      <Button
        icon={upvoted ? <CheckOutlined /> : <LikeOutlined />}
        type={upvoted ? "primary" : "default"}
        loading={processUpvote}
        onClick={upvote}
      >
        <span style={{ opacity: "0.5", marginLeft: isFeed ? "" : "8px" }}>
          {upvotes}
        </span>
      </Button>
    </Tooltip>

    <Tooltip title="Comments">
      <Button onClick={openDescriptionModal} type="default">
        <Badge count={commentsCount || 0} dot>
          <MessageOutlined style={{ color: "#333" }} />
        </Badge>
      </Button>
    </Tooltip>
  </Button.Group>
);

export default UpvoteButtonGroup;
