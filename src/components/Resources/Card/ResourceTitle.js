import React from "react";
import { Typography, Tag } from "antd";
// CSS
import "../Item.css";

const { Text } = Typography;

const ResourceTitle = ({
  isFeed,
  isMobileOnly,
  openDescriptionModal,
  resource,
}) => {
  return (
    <div style={{ paddingLeft: "16px" }}>
      <Typography.Title
        level={4}
        className="resources-card-title resource-card-clickable"
        style={{ fontSize: "1rem", marginBottom: "0em" }}
        onClick={openDescriptionModal}
      >
        {resource.title && resource.title}
      </Typography.Title>

      <p className="resources-card-tagline">
        {resource.tagline && resource.tagline}
      </p>

      {isFeed && !isMobileOnly && (
        <React.Fragment>
          <div
            style={{
              marginTop: "7px",
              marginBottom: "13px",
              whiteSpace: "nowrap",
            }}
          >
            {resource.tags &&
              resource.tags.map(tag => <Tag key={tag}>{tag}</Tag>)}
          </div>

          <div className="resources-details-wrapper">
            <Text type="secondary">
              {resource.authorName ? resource.authorName : "Anonymous"}
              {resource.affiliation &&
                resource.affiliation !== "None" &&
                `, ${resource.affiliation}`}
            </Text>
          </div>
        </React.Fragment>
      )}
    </div>
  );
};

export default ResourceTitle;