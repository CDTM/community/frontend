import React from "react";
import "../Item.css";

const ResourceImage = ({ onClick, image, isFeed, isMobileOnly }) => (
  <div
    style={{
      height: isFeed && !isMobileOnly ? "7.5rem" : "3.5rem",
      width: isFeed && !isMobileOnly ? "7.5rem" : "3.5rem",
      marginLeft: isFeed && !isMobileOnly ? "0px" : "0",
    }}
  >
    <div
      className="resources-logo-wrapper"
      onClick={onClick}
      role="presentation"
    >
      <img src={image} alt="Resource Logo" className="resources-logo" />
    </div>
  </div>
);

export default ResourceImage;
