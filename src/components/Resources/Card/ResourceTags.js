import React from "react";
import { Tag } from "antd";

const ResourceTags = ({ tags }) => {
  return (
    <div style={{ marginBottom: "12px", whiteSpace: "nowrap", width: "100%", overflowX: "auto"}}>
      {tags.map(tag => (
        <Tag key={tag}>{tag}</Tag>
      ))}
    </div>
  );
};

export default ResourceTags;
