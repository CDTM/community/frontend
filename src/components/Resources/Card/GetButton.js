import React from "react";
import { Tooltip, Button } from "antd";
import { LinkOutlined } from "@ant-design/icons";

const GetButton = ({ url }) => (
  <Tooltip
    title={`Open ${url && url.replace(/^https?:\/\//, "").replace(/\/$/, "")}`}
    placement="topRight"
  >
    <Button
      icon={<LinkOutlined />}
      onClick={event => {
        event.stopPropagation();
        window.open(url.replace(/\/$/, ""));
      }}
    >
      <span>Get</span>
    </Button>
  </Tooltip>
);

export default GetButton;
