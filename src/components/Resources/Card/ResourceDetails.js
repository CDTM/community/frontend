import React from "react";
import { Typography } from "antd";

const { Text } = Typography;

const ResourceDetails = ({ resource }) => {
  return (
    <div className="resources-details-wrapper">
      <Text type="secondary">
        {resource.authorName ? resource.authorName : "Anonymous"}
        {resource.affiliation &&
          resource.affiliation !== "None" &&
          `, ${resource.affiliation}`}
      </Text>
      <Text type="secondary">
        {resource.createdAt.toDate().toLocaleDateString("en-EN", {
          year: "numeric",
          month: "long",
          day: "numeric",
        })}
      </Text>
    </div>
  );
};

export default ResourceDetails;
