import React from "react";
import { Popconfirm, Tooltip } from "antd";
import { DeleteOutlined } from "@ant-design/icons";

const DeleteIcon = ({ resource, firebase, refreshResources }) => {
  const deleteResource = async event => {
    event.stopPropagation();

    try {
      await resource.ref.delete();
    } catch {
      return;
    }

    firebase.logEvent("resources_delete_resource", {
      title: resource.title,
      type: resource.type,
    });

    refreshResources();
  };

  return (
    <Popconfirm
      title={
        <p>
          Do you want to delete this resource?
          <br />
          This action cannot be undone.
        </p>
      }
      cancelText="Cancel"
      okText="Delete"
      okType="danger"
      onConfirm={deleteResource}
      onClick={event => event.stopPropagation()}
      onCancel={event => event.stopPropagation()}
    >
      <Tooltip title="Delete resource">
        <DeleteOutlined
          className="resources-action-icon"
          style={{ marginRight: "8px" }}
        />
      </Tooltip>
    </Popconfirm>
  );
};

export default DeleteIcon;
