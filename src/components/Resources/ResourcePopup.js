import React, { Component } from "react";
import {
  Typography,
  Tag,
  Modal,
  List,
  Comment,
  Form,
  Input,
  Button,
  message,
  Popconfirm,
  Tooltip,
  Row,
  Col,
  Avatar,
  Space,
} from "antd";
import { isMobileOnly } from "react-device-detect";
import InfiniteScroll from "react-infinite-scroller";
import { LoadingOutlined, DeleteOutlined, LikeOutlined, CheckOutlined } from "@ant-design/icons";
import { withFirebase } from "../Firebase";
import { defaultAvatarUrl } from "../Avatar";
import { ProfileDrawer } from "../Directory/ProfileDrawer"
import GetButton from "./Card/GetButton";

const { Text } = Typography;
const { TextArea } = Input;

const resourceText = (resource) => (
  <div>
    <div style={{ marginBottom: "4px" }}>
      <Text strong>{resource.tagline && resource.tagline}</Text>
    </div>
    <div>{resource.description && resource.description}</div>
    <div style={{ margin: "18px 0px" }}>
    {resource.tags &&
      resource.tags.map(tag => <Tag key={tag}>{tag}</Tag>)}
    </div>
  </div>
)

class ResourcePopup extends Component {
  constructor(props) {
    super(props);

    this.state = {
      comments: [],
      commentsLoading: props.resource.commentsCount > 0,
      currentCommentsCount: props.resource.commentsCount || 0,
      submittingComment: false,
      commentValue: "",
      pushingDelete: null,
      author: null,
      showProfileDrawer: false,
      profileSelected: null,
      upvoters: [],
      upvotersLoading: false,
      currentUser: null,
      hasMoreUpvoters: true,
    };

    this.onCloseProfileDrawer = this.onCloseProfileDrawer.bind(this)
  }

  async componentDidMount() {
    const { firebase, resource, authUser } = this.props;
    const { id: resourceId } = resource;

    try {
      const author = await firebase
        .users()
        .doc(resource.author)
        .get();
      this.setState({ author });
    } catch (e) {
      // console.error(`Could not fetch comments: ${e}`);
      this.setState({ author: null });
    }

    const currentUser = await firebase.users().doc(authUser.email).get()
    this.setState({currentUser})

    await this.initialLoadUpvoters()

    if (!resource.commentsCount || resource.commentsCount === 0) return;

    let commentsSnapshot;
    try {
      commentsSnapshot = await firebase
        .resourceComments(resourceId)
        .orderBy("createdAt", "asc")
        .get();
    } catch (e) {
      // console.error(`Could not fetch comments: ${e}`);
      this.setState({ commentsLoading: false });
      return;
    }

    const comments = [];
    commentsSnapshot.forEach(async (doc) => {
      // parse date here to be able to print "now" for newly created comments
      const data = doc.data();
      data.createdAt = data.createdAt.toDate().toLocaleDateString("en-EN", {
        year: "numeric",
        month: "long",
        day: "numeric",
        hour: "numeric",
        minute: "numeric",
        hour12: false,
      });

      const comment = {
        id: doc.id,
        ref: doc.ref,
        ...data,
      };
      comments.push(comment);
    });

    this.setState({
      comments,
      currentCommentsCount: commentsSnapshot.size,
      commentsLoading: false,
    });
  }

  onCloseProfileDrawer() {
    this.setState({
      profileSelected: null,
      showProfileDrawer: false,
    });
  }

  initialLoadUpvoters = async () => {
    const { firebase, resource } = this.props;

    if(resource.upvotes > 0){
      this.setState({upvotersLoading: true})
      const upvoters = []
      await resource.upvoters.forEach(async (id, index) => {
        let limit = 20
        if(resource.upvotes <= limit){
          limit = resource.upvotes
          this.setState({hasMoreUpvoters: false})
        }
        if(index < limit){
          try{
            const user = await firebase.users().doc(id).get()
            upvoters.push(user)
            if(limit === upvoters.length) this.setState({upvoters, upvotersLoading: false})
            // this.setState(upvoters)
          }
          catch(e){
            this.setState({upvoters: []})
          }
        }
      })
    }
  }

  loadMoreUpvoters = async () => {
    const { firebase, resource } = this.props;
    const {upvoters} = this.state

    const alreadyLoaded = upvoters.length
    if(resource.upvotes > alreadyLoaded){
      this.setState({upvotersLoading: true})
      await resource.upvoters.forEach(async (id, index) => {
        let limit = 6 + alreadyLoaded
        if(resource.upvotes <= limit){
          limit = resource.upvotes
          this.setState({hasMoreUpvoters: false})
        }
        if(index < limit && index >= alreadyLoaded){
          try{
            const user = await firebase.users().doc(id).get()
            upvoters.push(user)
            if(limit === upvoters.length) this.setState({upvoters, upvotersLoading: false})
            // this.setState(upvoters)
          }
          catch(e){
            this.setState({upvoters: []})
          }
        }
      })
    }
  }

  handleSubmit = async () => {
    const { commentValue } = this.state;
    const { firebase, resource, authUser } = this.props;
    const { id: resourceId } = resource;

    if (!commentValue || commentValue === "") {
      return;
    }

    this.setState({
      submittingComment: true,
    });

    let docRef;
    try {
      docRef = await firebase.doAddResourceComment(
        resourceId,
        authUser,
        commentValue,
      );
    } catch (e) {
      // console.error(e);
      message.error(`Error while posting comment: ${e}`);
      this.setState({ submittingComment: false });
      return;
    }

    this.setState(state => ({
      submittingComment: false,
      commentValue: "",
      currentCommentsCount: state.currentCommentsCount + 1,
      comments: [
        ...state.comments,
        {
          authorName:
            authUser.firstName &&
            authUser.lastName &&
            `${authUser.firstName} ${authUser.lastName}`,
          author: authUser.email,
          avatar: authUser.avatar,
          content: state.commentValue,
          id: docRef.id,
          ref: docRef,
          createdAt: "just now",
        },
      ],
    }));

    firebase.logEvent("resources_add_comment", {
      resourceId,
      resourceType: resource.type,
      resourceTitle: resource.title,
    });
  };

  handleChange = e => {
    this.setState({
      commentValue: e.target.value,
    });
  };


  deleteComment = async commentId => {
    const { firebase, resource } = this.props;
    const { id: resourceId } = resource;

    this.setState({ pushingDelete: commentId });

    try {
      await firebase
        .resourceComments(resourceId)
        .doc(commentId)
        .delete();
    } catch (e) {
      message.error(`Encountered error while deleting comment: ${e}`);
      this.setState({ pushingDelete: null });
      return;
    }

    this.setState(state => ({
      pushingDelete: null,
      currentCommentsCount: state.currentCommentsCount - 1,
      comments: state.comments.filter(item => item.id !== commentId),
    }));

    firebase.logEvent("resources_delete_comment", {
      resourceId,
      resourceType: resource.type,
      resourceTitle: resource.title,
    });
  };

  async fetchCommentAuthor(author){
    const { firebase } = this.props;
    try {
      const authorDoc = await firebase
        .users()
        .doc(author)
        .get();
      this.participantSelected(authorDoc)
    } catch (e) {
      // console.error(`Could not fetch comments: ${e}`);
    }
  }

  participantSelected(participant) {
    this.setState({
      showProfileDrawer: true,
      profileSelected: participant,
    });
  }


  render() {
    const { resource, close, authUser, upvote, upvotes, processUpvote, upvoted } = this.props;
    const {
      comments,
      commentsLoading,
      currentCommentsCount,
      submittingComment,
      commentValue,
      pushingDelete,
      author,
      showProfileDrawer,
      profileSelected,
      upvoters,
      upvotersLoading,
      currentUser,
      hasMoreUpvoters,
    } = this.state;

    let upvotersWithUser = upvoters;
    if(currentUser){
      upvotersWithUser = upvotersWithUser.filter(upvoter => upvoter.id !== currentUser.id)
      if(upvoted) {upvotersWithUser.unshift(currentUser);}
    }

    return (
      <Modal
        title={resource.title}
        visible
        onCancel={() => close()}
        footer={false}
        width={!isMobileOnly && "75%"}
        centered
      >
        <Row>
          <Col justify="space-around" span={isMobileOnly ? 16: 4} style={{overflow: "hidden"}}>
            <img 
              src={resource.image} 
              alt="Resource Logo" 
              style={{ border: "1px solid #e8e8e8", height: 200, width: "100%", objectFit: "cover", borderRadius: 10}}/>
            <Comment
                author={resource.authorName}
                avatar={author ? <Avatar src={author.data().avatar} onClick={() => this.participantSelected(author)}/> : defaultAvatarUrl}
                datetime={resource.createdAt.toDate().toLocaleDateString("en-EN", {
                  year: "numeric",
                  month: "short",
                  day: "numeric",
                })}/>
          </Col>
          {!isMobileOnly && <Col span={12} style={{paddingLeft: 20}}>
              {resourceText(resource)}
          </Col>}
          <Col span={8} style={{paddingLeft: 20}}>
            <div style={{height: 200, overflowY: "auto"}}>
              <div style={{ marginBottom: "8px" }}>
              {upvotes === 1 ? <Text strong style={{paddingRight: 5}}>{`${upvotes} Upvote`}</Text> : 
              <Text strong style={{paddingRight: 5}}>{`${upvotes} Upvotes`}</Text>}
              </div>
              <InfiniteScroll
                initialLoad={false}
                pageStart={0}
                loadMore={this.loadMoreUpvoters}
                hasMore={!upvotersLoading && hasMoreUpvoters}
                useWindow={false}
                threshold ={5}
              >
              <List
              style={{width:"90%"}}
              split={false}
              grid={{
                gutter: 16,
                xs: 2,
                sm: 3,
                md: 4,
                lg: 5,
                xl: 6,
                xxl: 7,
              }}
              dataSource={upvotersWithUser}
              locale={{ emptyText: "Be the first one to upvote" }}
              loading={upvotersLoading}
              renderItem={item => (
                <List.Item>
                  <Tooltip title={item.data().firstName}>
                    <Avatar size="large" src={item.data().avatar} key={item.id} 
                      onClick={() => this.participantSelected(item)}
                      style={{cursor: "pointer"}}/>
                  </Tooltip>
                </List.Item>
              )}
              />
              </InfiniteScroll>
            </div>
            <div style={{paddingTop: "20px", float: "right"}}>
            <Space align="end">
            <Tooltip
                title={upvoted ? "Withdraw vote" : `Vote for ${resource.title && resource.title}`}
                placement="topLeft"
              >
                <Button
                  icon={upvoted ? <CheckOutlined /> : <LikeOutlined />}
                  type={upvoted ? "primary" : "default"}
                  loading={processUpvote}
                  onClick={upvote}
                >
                  <span style={{ opacity: "0.5", marginLeft: "8px" }}>
                    {upvotes}
                  </span>
                </Button>
              </Tooltip>
              <GetButton url={resource.url}/>
            </Space>
            </div>
          </Col>
        {isMobileOnly && resourceText(resource)}
        </Row>
        <List
          header={`${currentCommentsCount} ${
            currentCommentsCount === 1 ? "comment" : "comments"
          }`}
          itemLayout="horizontal"
          dataSource={comments}
          loading={commentsLoading}
          locale={{ emptyText: "Be the first one to comment" }}
          renderItem={item => (
            <List.Item key={item.id}>
              <Comment
                author={item.authorName}
                avatar={item.avatar ? <Avatar src={item.avatar} onClick={() => this.fetchCommentAuthor(item.author)}/> : defaultAvatarUrl}
                content={item.content}
                datetime={item.createdAt}
                key={item.id}
                actions={
                  item.author === authUser.email && [
                    <Popconfirm
                      title={
                        <p>
                          Do you want to delete this comment?
                          <br />
                          This action cannot be undone.
                        </p>
                      }
                      okText="Delete"
                      okType="danger"
                      onConfirm={() => this.deleteComment(item.id)}
                      disabled={pushingDelete}
                    >
                      <Tooltip title="Delete comment">
                        {pushingDelete === item.id ? (
                          <LoadingOutlined />
                        ) : (
                          <DeleteOutlined />
                        )}
                      </Tooltip>
                    </Popconfirm>,
                  ]
                }
              />
            </List.Item>
          )}
        />
        {!commentsLoading && (
          <Comment
            avatar={authUser.avatar}
            content={
              <React.Fragment>
                <Form.Item>
                  <TextArea
                    rows={4}
                    onChange={this.handleChange}
                    value={commentValue}
                    disabled={submittingComment}
                  />
                </Form.Item>
                <Form.Item>
                  <Button
                    htmlType="submit"
                    loading={submittingComment}
                    onClick={this.handleSubmit}
                    type="primary"
                  >
                    Add Comment
                  </Button>
                </Form.Item>
              </React.Fragment>
            }
          />
        )}
        <ProfileDrawer
          visible={showProfileDrawer}
          selectedProfile={profileSelected ? profileSelected.data() : null}
          onClose={this.onCloseProfileDrawer}
        />
      </Modal>
    );
  }
}

export default withFirebase(ResourcePopup);
