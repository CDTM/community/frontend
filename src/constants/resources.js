import React from "react";
import {
  StarOutlined,
  LikeOutlined,
  FieldTimeOutlined,
  SortAscendingOutlined,
} from "@ant-design/icons";

export const resourceTypes = {
  Tools: {
    tags: [
      "Project Management",
      "Teamwork",
      "Workflow",
      "Development",
      "Design",
      "Research & Analytics",
      "Legal",
      "Finance",
      "HR",
      "Personal Growth",
      "Sustainability",
      "Other",
    ],
  },
  Books: { 
    tags: [
      "Fiction", 
      "Business",
      "Startups",
      "Science",
      "Technology",
      "History",
      "Politics",
      "Society",
      "Personal Growth",
      "Sustainability", 
      "Other",
    ], 
  },
  Podcasts: { 
    tags: [
      "Entertainment", 
      "Business",
      "Startups",
      "Science",
      "Technology",
      "History",
      "Politics",
      "Society",
      "Personal Growth",
      "Sustainability", 
      "Other",
    ],
  },
  Articles: { 
    tags: [
      "Entertainment", 
      "Business",
      "Startups",
      "Science",
      "Technology",
      "History",
      "Politics",
      "Society",
      "Personal Growth",
      "Sustainability", 
      "Other",
    ],
  },
  Videos: {
    tags: [
      "Entertainment", 
      "Business",
      "Startups",
      "Science",
      "Technology",
      "History",
      "Politics",
      "Society",
      "Personal Growth",
      "Sustainability", 
      "Other",
    ],
  },
  Courses: { 
    tags: [
      "Project Management",
      "Development",
      "Design",
      "Research & Analytics",
      "Legal",
      "Finance",
      "HR",
      "Personal Growth",
      "Sustainability",
      "Other",
    ],
  },
  Scholarships: {
    tags: [
      "Financial Funding", 
      "Ideational Support", 
      "Abroad Financing",
      "Other",
    ],
  },
  Conferences: { 
    tags: [ 
      "Business",
      "Startups",
      "Science",
      "Technology",
      "History",
      "Politics",
      "Society",
      "Personal Growth",
      "Sustainability", 
      "Other",
    ],
   },
  "Startup Deals": {
    tooltip:
      "If you know of any deals you'd like to have and the CDTM can apply for, please reach out to entrepreneurshiptf@cdtm.de",
    tags: [
      "Software",
      "Infrastructure",
      "Productivity", 
      "Marketing", 
      "Other",
    ],
  },
};

export const sortOptions = {
  score: {
    label: "Relevance",
    order: "desc",
    icon: <StarOutlined />,
  },
  upvotes: {
    label: "Upvotes",
    order: "desc",
    icon: <LikeOutlined />,
  },
  createdAt: {
    label: "Submission",
    order: "desc",
    icon: <FieldTimeOutlined />,
  },
  title: {
    label: "Title",
    order: "asc",
    icon: <SortAscendingOutlined />,
  },
};
