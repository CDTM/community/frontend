# CDTM Community

## Links

Production: https://community.cdtm.de<br>
Development: https://develop--community-cdtm-de.netlify.com

## Local Development Setup

Clone down this repository. You will need node and npm installed globally on your machine. In addition you need an `.env` file that you can get from the maintainers of this project.

Installation of requirements:

`npm install`

To start the development server, it will automatically refresh the app when you make changes to the src files:

`npm start`

To visit app:

http://localhost:3000/

## Deployment

The project is directly deployed from master to [production](https://community.cdtm.de) using Netlify. All branches are also deployed at `https://<branch_name>--community-cdtm-de.netlify.app`. In addition, Netlify deploys a preview of merge requests at `https://deploy-preview-<merge_request_id>--community-cdtm-de.netlify.app`. Unfortunately, domains need to be whitelisted for Firebase Auth [here](https://console.firebase.google.com/project/cdtm-community-project/authentication/providers) which is reasonable for larger, work in progress merge requests and branches. Wildcard/Regex whitelisting is not possible yet according to Firebase support.

If deployment fails, a link to the log is in #community-platform-notifications on Slack. For merge requests, the domain and link to the log are added as a comment to the request.

## Backend

This project uses Firebase as a backend that allows for agile development. Firebase Auth, Firestore, Storage and Functions are used. Cloud functions and security rules (Firestore and Storage) are deployed from other repositories which are closed to protect the security logic. Please reach out to the maintainers of the project if you need changes.

The main Firestore database is backed up daily via a Cloud Function to the following bucket: `gs://cdtm-community-firestore-backups`

## Analytics

Some events are logged to Google Analytics via Firebase. Access to the Google Analytics project can be requested from the maintainers.

## Contributing

Everyone is welcomed to contribute to this community platform. Some guiding principles and first steps are outlined in [CONTRIBUTING.md](CONTRIBUTING.md). If you have any other question, feel free to reach out to the maintainers.
